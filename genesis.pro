#-------------------------------------------------
#
# Project created by QtCreator 2019-05-08T13:56:02
#
#-------------------------------------------------

greaterThan(QT_MAJOR_VERSION, 5): QT       += core gui

QT +=network widgets
TARGET = genesis
TEMPLATE = app
QMAKE_LFLAGS += -no-pie
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++14 console

SOURCES += \
    Font/font.cpp \
    Listener/listener.cpp \
    OpenGL/Camera3D/Transform3D/transform3d.cpp \
    OpenGL/Camera3D/camera3d.cpp \
    OpenGL/glview.cpp \
    OpenGL/input/input.cpp \
        main.cpp \
        mainwindow.cpp \
    world/Perlin/perlin2d.cpp \
    world/Point/point.cpp \
    world/world.cpp

HEADERS += \
    Font/font.h \
    Listener/listener.h \
    OpenGL/Camera3D/Transform3D/transform3d.h \
    OpenGL/Camera3D/camera3d.h \
    OpenGL/glview.h \
    OpenGL/input/input.h \
        mainwindow.h \
    world/Perlin/perlin2d.h \
    world/Point/point.h \
    world/world.h


FORMS += \
        dialog.ui \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resourse.qrc
INCLUDEPATH = /home/alex/Qt/proj/GLM/
DISTFILES +=
CONFIG += link_pkgconfig
PKGCONFIG += freetype2 x11
LIBS += -lglfw3 -ldl
#LIBS += -lglfw3
#unix: PKGCONFIG += glfw3
