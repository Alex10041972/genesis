#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QDebug>
#include <QDateTime>
#include <QRadioButton>
#include <QStyle>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    dl(new Ui::Dialog)
{
    ui->setupUi(this);
    qInfo() << "main thread:" << QThread::currentThreadId();

    qRegisterMetaType<QList<point*>>("QList<point*>");

    QProgressBar *progress = new QProgressBar();
    ui->statusBar->addWidget(progress,ui->centralWidget->width());
    qsrand( static_cast<uint>(QDateTime::currentMSecsSinceEpoch()));
    map = new World;
    QThread* Wthread = new QThread;


    ui->labelOctava->setNum(ui->sliderOctave->value());
    connect(ui->sliderOctave,SIGNAL(sliderMoved(int)),ui->labelOctava,SLOT(setNum(int)));

    ui->labelFreq->setNum(ui->sliderFreq->value());
    connect(ui->sliderFreq,SIGNAL(sliderMoved(int)),ui->labelFreq,SLOT(setNum(int)));
    connect(ui->sliderFreq,SIGNAL(sliderMoved(int)),map,SLOT(setFreq(int)));

    ui->labelAmplitude->setNum(ui->sliderFreq->value());
    connect(ui->sliderAmplitude,SIGNAL(sliderMoved(int)),ui->labelAmplitude,SLOT(setNum(int)));

    map->moveToThread(Wthread);
    map->setHW(HEIGHT,WIDTH);
    map->setFreq(ui->sliderFreq->value());

    connect(Wthread, SIGNAL(started()), map, SLOT(start()));
    connect(map,SIGNAL(progress(int)),progress,SLOT(setValue(int)));

    connect(ui->seed,SIGNAL(valueChanged(int)),map,SLOT(setSeed(int)));
    setSeed();

    connect (ui->randomSeed,SIGNAL(clicked()),this,SLOT(setSeed()));

    connect(ui->startCreateWorld,SIGNAL(clicked()),map,SLOT(start()));
    connect(map,SIGNAL(endCreateMap(QList<point*>)),this,SLOT(draw(QList<point*>)),Qt::QueuedConnection);
    ui->openGLWidget->setHW(HEIGHT,WIDTH);
    Wthread->start();

    list.on_starting_clicked();

}

MainWindow::~MainWindow()
{
    delete map;
    delete ui;
}

//int MainWindow::getSeed()
//{
//    return ;
//}

void MainWindow::setSeed()
{
    ui->seed->setValue(static_cast<int>(qrand()%999999999));
}

void MainWindow::draw(QList<point*> map)
{
    ui->openGLWidget->draw_point(map);
}



void MainWindow::on_action_triggered()
{
    if (ui->action->isChecked()) {
        QDialog *widget = new QDialog(this);
        dl->setupUi(widget);
        connect(dl->rbs,SIGNAL(clicked()),this,SLOT(cheket()));
        connect(dl->rbc,SIGNAL(clicked()),this,SLOT(cheket()));
        widget->show();
        connect(dl->ip,SIGNAL(textChanged(QString)),this,SLOT(ipValidator(QString)));
        connect(dl->port,SIGNAL(textChanged(QString)),this,SLOT(portValidator(QString)));
    }
}
void MainWindow::cheket(){

    if (dl->rbc->isChecked())
    {
        dl->l_ip->show();
        dl->ip->show();
    }
    if (dl->rbs->isChecked())
    {
        dl->l_ip->hide();
        dl->ip->hide();
    }
}
void MainWindow::ipValidator(QString str){
    QRegExp rx("^[0-9]{3}['.']{1}[1-9]{3}['.']{1}[1-9]{3}['.']{1}[1-9]{3}$");
    QValidator *validator = new QRegExpValidator(rx, this);
    int pos = 0;
    dl->ip->setValidator(validator);
    if (1 == validator->validate(str,pos))
    {
        dl->ip->setStyleSheet("QLineEdit {border: 3px solid red}");
    } else  if (2 == validator->validate(str,pos)) {
        dl->ip->setStyleSheet("QLineEdit {border: 3px solid green}");
    }


}

void  MainWindow::portValidator(QString str) {
    QRegExp rx("^[0-9]{5}$");
    QValidator *validator = new QRegExpValidator(rx, this);
    int pos = 0;
    dl->port->setValidator(validator);
    if (1 == validator->validate(str,pos))
    {
        dl->port->setStyleSheet("QLineEdit {border: 3px solid red}");
    } else  if (2 == validator->validate(str,pos)) {
        dl->port->setStyleSheet("QLineEdit {border: 3px solid green}");
    }
}
