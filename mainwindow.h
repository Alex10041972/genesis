#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "world/world.h"
//#include "OpenGL/painter.h"
#include <QProgressBar>
#include <QLabel>
#include <QLayout>
#include "OpenGL/glview.h"
#include "Listener/listener.h"
//#define OCTAVE 3
//#define FREQ 25
#include "ui_dialog.h"
#define HEIGHT 200
#define WIDTH  200
namespace Ui {
class MainWindow;

}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QLabel *label;


private:

    Ui::MainWindow *ui;
    World* map;
    Glview *openGLWidget;
    Listener list;

    Ui::Dialog *dl;

public:
signals:
    void started();
public slots:
    void draw(QList<point *> i);
    void setSeed();


private slots:
    void on_action_triggered();
    void cheket();
    void ipValidator(QString str);
    void  portValidator(QString str);
};

#endif // MAINWINDOW_H
