#ifndef GLVIEW_H
#define GLVIEW_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLDebugLogger>

#include <QVector3D>
#include <QMatrix4x4>
#include "Camera3D/Transform3D/transform3d.h"
#include "Camera3D/camera3d.h"
#include <ft2build.h>
#include FT_FREETYPE_H

//
//#include "map.h"
#include "../world/world.h"
class QOpenGLShaderProgram;

class Glview : public QOpenGLWidget,
        protected QOpenGLFunctions
{
    Q_OBJECT
public:
    Glview(QWidget *parent = nullptr) :
        QOpenGLWidget(parent)
    {
        m_transform.rotate(1.0f, QVector3D(0.4f, 0.3f, 0.3f));
        fov=45.0f;
        far = 1000;
    }

    void draw_point(QList<point *> i);
    void setHW(int h,int w);


    void paintArr();
    void text(double nbFrames);

    void cube();

    QVector3D getColor(float level);

private:
    ///////////
    QOpenGLBuffer m_vertex; // VBO
    QOpenGLVertexArrayObject m_object;
    QOpenGLShaderProgram *m_program;
    QOpenGLBuffer _matbo;
    //
    GLuint _matrixAttr;

    void printVersionInformation();
    ///////////

    ///
    ///
    ///
    ///
    ///
    QVector<QVector3D> triangles;

    float zoom=0.0f;
    GLfloat currentWidth;
    GLfloat currentHeight;
    int sizeX,sizeY;


    double currentTime; // время для подсчета FPS
    double lastTime; // // время для подсчета FPS
    int nbFrames = 0; //  для подсчета FPS
    int fps;
    GLfloat fov;
    QList<point*> points;
    GLfloat cnt1;
    GLfloat cnt2;

    //    void mouseMoveEvent(QMouseEvent* event) override;
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void wheelEvent(QWheelEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

void scroll_callback( double xoffset, double yoffset);
void paintQ();
GLfloat far;// = 1000;
    int u_modelToWorld;
    int u_worldToCamera;
    int u_cameraToView;
    QMatrix4x4 m_projection;
    Camera3D m_camera;
    Transform3D m_transform;



signals:
    //        void signal();

public    slots:
    void updater();

};

#endif // GLVIEW_H
