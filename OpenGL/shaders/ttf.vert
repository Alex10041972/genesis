#version 440
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 colour;
smooth out vec4 col;
uniform mat4 modelToWorld;
uniform mat4 worldToCamera;
uniform mat4 cameraToView;
void main()
{
  gl_Position =vec4(position,1.0);
  col = vec4(colour,1.0);
}
