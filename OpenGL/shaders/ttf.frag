#version 440
in vec4 col;
out vec4 fColor;

void main()
{
    if (fColor.r == col.r){
        fColor = vec4(0.0,0.0,0.0,1);
    }else{
        fColor = vec4(1.0,0.0,1.0,1);
    }
}
