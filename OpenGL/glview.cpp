﻿#include "glview.h"
#include <QDebug>


#include <QOpenGLShaderProgram>
#include "OpenGL/input/input.h"

#include <ctime>
// GLEW
//#define GLEW_STATIC
//#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// Other Libs
//#include <SOIL.h>
// GLM Mathematics
//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>

#include "Font/font.h"
#include <ft2build.h>
#include <math.h>
#include <SOIL.h>
#include <QDataStream>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include <iostream>
#include <QTextCodec>
#define OCEAN 0.0f
#define BEACH 0.10f
#define TAIGA 0.30f

#define GRASSLAND 0.45f
#define TROPICAL_RAIN_FOREST 0.50f
#define SNOW 0.65f
using std::string;
using std::wstring;
using std::cout;
using std::cin;
static float z_max = 0.0f;
QVector3D Glview::getColor(float level)
{
    //qDebug()<<level;
    QVector3D color; // color -- вынести в хедер для того чтоб не выделять память каждый раз или чтото сделать с этим !
    //  скорее всего перенести в другой класс
    // А СКОРЕЕ ВСЕГО В В WORLD как и создание масива треугольников !

    float  e = level;
if (e > z_max) {z_max = e;}
    if (e < OCEAN) {
        color.setX(68/255.0f);
        color.setY(68/255.0f);
        color.setZ(e+1);

    } else if (e < BEACH ) {
        color.setX(160/255.0f);
        color.setY(144/255.0f);
        color.setZ(119/255.0f);

    } else  if (e < GRASSLAND) {
        color.setX(84/255.0f);
        color.setY(0.800f-e);
        color.setZ(68/255.0f);


    }else if (e < TROPICAL_RAIN_FOREST) {
        color.setX(92/255.0f);
        color.setY(154/255.0f);
        color.setZ(89/255.0f);


    } else  if (e < TAIGA) {
        color.setX(68/255.0f);
        color.setY(136/255.0f);
        color.setZ(85/255.0f);

    } else {
        color.setX(221/255.0f);
        color.setY(221/255.0f);
        color.setZ(228/255.0f);
    }
    return color;
}

void Glview::paintArr()
{

    if (points.length()){
        triangles.clear();
        //        qDebug()<<"sizeX:"<<sizeX<<"sizeY:"<<sizeY<<points.length()<<points[0]->p;
        for (int y=0;y<sizeY-1;y++){
            for (int x= 0 ;x<sizeX-1;x++) {
                triangles<<QVector3D(points[sizeX*y+x+1]->p);
                triangles<<QVector3D(getColor(points[sizeX*y+x+1]->p.z()));

                triangles<<QVector3D(points[sizeX*y+x+1+sizeX]->p);
                triangles<<QVector3D(getColor(points[sizeX*y+x+1+sizeX]->p.z()));

                triangles<<QVector3D(points[sizeX*y+x]->p);
                triangles<<QVector3D(getColor( points[sizeX*y+x]->p.z()));

                triangles<<QVector3D(points[sizeX*y+x]->p);
                triangles<<QVector3D(getColor(points[sizeX*y+x]->p.z()));

                triangles<<QVector3D(points[sizeX*y+x+1+sizeX]->p);
                triangles<<QVector3D(getColor(points[sizeX*y+x+1+sizeX]->p.z()));

                triangles<<QVector3D(points[sizeX*y+x+sizeX]->p);
                triangles<<QVector3D(getColor(points[sizeX*y+x+sizeX]->p.z()));
            }
        }
    }
//    if (0.0f != z_max){m_camera.translate(1.0,1.0,z_max);m_transform.scale(3.0,3.0,3.0);};

    m_vertex.bind();
    m_vertex.allocate(triangles.data(),static_cast<int>(sizeof(QVector3D))*triangles.count());
    m_vertex.release();
    update();
}
void Glview::paintQ()
{


}

void Glview::text(double nbFrames  )
{
    // This holds all the information for the font that we are going to create.
    freetype::font_data our_font;
    our_font.init("../genesis/Font/ArialRegular.ttf",16);


    //     Clear Screen And Depth Buffer
    glLoadIdentity();                                                                       // Reset The Current Modelview Matrix
    glTranslatef(0.0f,0.0f,-1.0f);                                          // Move One Unit Into The Screen
    // Blue text
    glColor4f(0.5,0.5,0.5,1.0f);
    //    glColor3f(1.0f*float(cos(cnt1)),1.0f*float(sin(cnt2)),1.0f-0.5f*float(cos(cnt1+cnt2)));
    //            // Position the WGL Text On The Screen
    //    glRasterPos2f(-0.0, 0.35f);
    //                glPrint("Active WGL Bitmap Text With NeHe - %7.2f");      // Print GL Text To The Screen

    //Here we print some text using our freetype font
    //The only really important command is the actual print() call,
    //but for the sake of making the results a bit more interesting
    //I have put in some code to rotate and scale the text.
    glPushMatrix();
    //    glLoadIdentity();
    //    glRasterPos2f(-0.45f+0.05f*float(cos(cnt1)), 0.35f*float(sin(cnt2)));
    glRotatef(cnt1,0,0,1);
    //    glScalef(1,.8f+ .3f*cos(cnt2),1);
    glTranslatef(5,5,0);
    //        glTranslatef(0,0,0);
    QString str = "FPS "+ QString::number(nbFrames)+"\n"; //str.toStdString().c_str()

    freetype::print(  our_font, 0, 0, str.toStdString().c_str(), static_cast<double>(cnt2) );

    //Uncomment this to test out print's ability to handle newlines.
    //    freetype::print(our_font, 10,10,"FPS - %0.3f\nffфывап\n", static_cast<double>(cnt2));
    glPopMatrix();
    //    cnt1+=0.051f;                                                                           // Increase The First Counter
    //    cnt2+=0.05f;
    //    if (1<cnt1)cnt1=0.0;
    // Increase The First Counter
    our_font.clean();
    //    qDebug()<<"out-text";
    currentTime = clock();
}



void Glview::printVersionInformation()
{
    QString glType;
    QString glVersion;
    QString glProfile;

    // Get Version Information
    glType = (context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL";
    glVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));

    // Get Profile Information
#define CASE(c) case QSurfaceFormat::c: glProfile = #c; break
    switch (format().profile())
    {
    CASE(NoProfile);
    CASE(CoreProfile);
    CASE(CompatibilityProfile);
    }
#undef CASE

    // qPrintable() will print our QString w/o quotes around it.
    qDebug() << qPrintable(glType) << qPrintable(glVersion) << "(" << qPrintable(glProfile) << ")";
}


void Glview::initializeGL()

{
    glfwInit();
    //    double currentTime = glfwGetTime();
    lastTime  = glfwGetTime();   // незнаю надо ли заменить на ctime() -
    nbFrames = 0;
    cnt1=0.0f;
    cnt2=0.0f;
    initializeOpenGLFunctions();
    printVersionInformation();
    connect(this, SIGNAL(frameSwapped()), this, SLOT(updater()));

    // Set global information
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    {
        // Create Shader (Do not release until VAO is created)
        m_program = new QOpenGLShaderProgram();
        m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, "/home/alex/Qt/proj/game/genesis/OpenGL/shaders/simple.vert");
        m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, "/home/alex/Qt/proj/game/genesis/OpenGL/shaders/simple.frag");
        m_program->link();
        u_modelToWorld = m_program->uniformLocation("modelToWorld");
        u_worldToCamera = m_program->uniformLocation("worldToCamera");
        u_cameraToView = m_program->uniformLocation("cameraToView");
        m_program->bind();

        // Create Buffer (Do not release until VAO is created)
        m_vertex.create();
        m_vertex.bind();
        m_vertex.setUsagePattern(QOpenGLBuffer::StaticDraw);


        //        m_vertex.allocate(triangles.data(),static_cast<int>(sizeof(QVector3D))*triangles.count());
        // Create Vertex Array Object
        m_object.create();
        m_object.bind();
        m_program->enableAttributeArray(0);
        m_program->enableAttributeArray(1);
        m_program->setAttributeBuffer(0, GL_FLOAT, 0, 3, sizeof(QVector3D)*2);
        m_program->setAttributeBuffer(1, GL_FLOAT,sizeof(QVector3D), 3, sizeof(QVector3D)*2);

        // Release (unbind) all
        m_object.release();
        m_vertex.release();
        m_program->release();
    }
//    m_transform.scale(20.0f,20.0f,20.0f);
//    m_transform.rotate(40,QVector3D(1.0,1.0,1.0));
//    m_transform.rotate(180,QVector3D(1.0,0.0,0.0));
//    qDebug()<<m_transform;
    //    m_transform.rotate(40)
}

void Glview::resizeGL(int w,int h)
{

    //    glMatrixMode(GL_PROJECTION);
    //    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //    glLoadIdentity();



    //    glViewport(0, 0, w, h);
    qDebug()<<"resize";
    currentWidth = w;
    currentHeight = h;
    //    m_projection.setToIdentity();
    //    m_projection.perspective(60.0f, w / float(h), 1000.0f, 1000.0f);
//        m_projection.scale(2.0,4.0,4.0);
}



void Glview::paintGL()
{
    //    nbFrames++;
    //    if ( (currentTime - lastTime)/CLOCKS_PER_SEC >= 1.0 ){ // If last prinf() was more than 1sec ago
    //        // printf and reset
    //        fps = nbFrames;
    //        printf("%f ms/frame  ftame: %f \n", CLOCKS_PER_SEC/double(nbFrames),double(nbFrames));
    //        nbFrames = 0;
    //        lastTime = currentTime;
    //    }

    // Clear

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    //    glEnable(GL_CULL_FACE);
//    m_projection.perspective(0.001f, (GLfloat)currentWidth/(GLfloat)currentHeight, 0.001f, 0.001f);

    // Render using our shader

    m_program->bind();
    m_program->setUniformValue(u_worldToCamera, m_camera.toMatrix());
    m_program->setUniformValue(u_cameraToView, m_projection);
    {
        m_object.bind();
        m_program->setUniformValue(u_modelToWorld, m_transform.toMatrix());
        glDrawArrays(GL_TRIANGLES,0,triangles.count()/2);
        m_object.release();
    }

    m_program->release();
    double currentTime = glfwGetTime();
    nbFrames++;
    if ( currentTime - lastTime >= 1.0 ){ // If last prinf() was more than 1 sec ago
        // printf and reset timer
        //        printf("%f ms/frame\n", 1000.0/double(nbFrames));
        fps = 1000/  nbFrames;// = 0;
        nbFrames = 0;
        lastTime += 1.0;


    }
    //qDebug()<<m_transform;
    text(fps);
}


void Glview::draw_point(QList<point *> i)
{
    this->points= i;
    paintArr();
    update();
}

void Glview::setHW(int h, int w)
{
    sizeX=w;
    sizeY=h;
}

void Glview::wheelEvent(QWheelEvent *event)
{
    //xAxisRotation += (10 * (static_cast<GLfloat>(event->y()) - static_cast<GLfloat>(pressPosition.y()))) / (currentHeight);

    if (event->type() == QEvent::Wheel) {
        zoom = event->angleDelta().ry()/12000.0f;
        update();
        //        qDebug()<<event->angleDelta().ry()/12000.0f;
    }

}



void Glview::keyPressEvent(QKeyEvent *event)
{
    qDebug()<<"keyPressEvent";
    if (event->isAutoRepeat())
    {
        event->ignore();
    }
    else
    {
        Input::registerKeyPress(event->key());
    }
}

void Glview::keyReleaseEvent(QKeyEvent *event)
{
    qDebug()<<"keyReleaseEvent";
    if (event->isAutoRepeat())
    {
        event->ignore();
    }
    else
    {
        Input::registerKeyRelease(event->key());
    }
}

void Glview::mousePressEvent(QMouseEvent *event)
{
    qDebug()<<"mousePressEvent";
    Input::registerMousePress(event->button());
}

void Glview::mouseReleaseEvent(QMouseEvent *event)
{
    qDebug()<<"mouseReleaseEvent";
    Input::registerMouseRelease(event->button());
}

void Glview::updater()
{


    // Update input
    Input::update();

    // Camera Transformation
    if (Input::buttonPressed(Qt::RightButton))
    {
        static const float transSpeed = 0.5f;
        static const float rotSpeed   = 0.5f;

        // Handle rotations
        m_camera.rotate(-rotSpeed * Input::mouseDelta().x(), Camera3D::LocalUp);
        m_camera.rotate(-rotSpeed * Input::mouseDelta().y(), m_camera.right());

        // Handle translations
        QVector3D translation;
        if (Input::keyPressed(Qt::Key_W))
        {
            translation += m_camera.forward();
        }
        if (Input::keyPressed(Qt::Key_S))
        {
            translation -= m_camera.forward();
        }
        if (Input::keyPressed(Qt::Key_A))
        {
            translation -= m_camera.right();
        }
        if (Input::keyPressed(Qt::Key_D))
        {
            translation += m_camera.right();
        }
        if (Input::keyPressed(Qt::Key_Q))
        {
            translation -= m_camera.up();
        }
        if (Input::keyPressed(Qt::Key_E))
        {
            translation += m_camera.up();
        }
        if (Input::keyPressed(Qt::Key_Z))
        {
//            qDebug()<<m_camera<<translation.toVector4D();
            ;
        }

        m_camera.translate(transSpeed * translation);
    }

    // Update instance information
    update();
}
void Glview::scroll_callback(double xoffset, double yoffset)
{
    if (fov >= 1.0f && fov <= 45.0f)
        fov -= yoffset;
    if (fov <= 1.0f)
        fov = 1.0f;
    if (fov >= 45.0f)
        fov = 45.0f;
}
