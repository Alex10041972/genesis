#ifndef LISTENER_H
#define LISTENER_H

#include <QObject>
#include <QtNetwork>
#include <QTcpSocket>
#include <QObject>
#include <QByteArray>
#include <QDebug>

class Listener : public QObject
{
    Q_OBJECT
public:
    explicit Listener(QObject *parent = nullptr);

public
    slots:

//    void on_stoping_clicked();
    void on_starting_clicked();
    void newuser();
//    void slotReadClient();

private:
    QTcpServer *tcpServer;
    int server_status;
    QMap<int,QTcpSocket *> SClients;
};

#endif // LISTENER_H
