#include "listener.h"

Listener::Listener(QObject *parent) : QObject(parent)
{

}

void Listener::on_starting_clicked()
{
    tcpServer = new QTcpServer(this);
        connect(tcpServer, SIGNAL(newConnection()), this, SLOT(newuser()));
        if (!tcpServer->listen(QHostAddress::Any, 33333) && server_status==0) {
            qDebug() <<  QObject::tr("Unable to start the server: %1.").arg(tcpServer->errorString());
        } else {
            server_status=1;
            qDebug() << tcpServer->isListening() << "TCPSocket listen on port";
            qDebug() << QString::fromUtf8("Сервер запущен!");
        }
}

void Listener::newuser()
{
    if(server_status==1){
            qDebug() << QString::fromUtf8("У нас новое соединение!");
            QTcpSocket* clientSocket=tcpServer->nextPendingConnection();
            int idusersocs=clientSocket->socketDescriptor();
            SClients[idusersocs]=clientSocket;
            qDebug()<<clientSocket->peerAddress();
            connect(SClients[idusersocs],SIGNAL(readyRead()),this, SLOT(slotReadClient()));
        }
}
