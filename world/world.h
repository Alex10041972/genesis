#ifndef WORLD_H
#define WORLD_H


#include <QList>
#include "Perlin/perlin2d.h"
#include "Point/point.h"
#include <QObject>
#include <QPointF>

class World:public QObject
{
    Q_OBJECT
private:
    int x;   // height
    int y;   // width

    //    PerlinNoise *perlin;
    //    QThread *tread;
public:
    explicit World(QObject *parent = nullptr);
    explicit  World(int height, int width);
    Perlin2D  perlin;

    ~World();
    QList <point*> map;
    float freq;
    void setHW(int height, int width);
    int getH(){
        return x;
    }
    int getW(){
        return y;
    }
    void bls();
    int zzzz;
    float getFreq();
    QVector3D pixelPosToViewPos(const int pos_x,const int pos_y, const int i)
    {
        return QVector3D(2.0f * pos_x / (x-1)-1.0f,1- 2.0f * pos_y / (y-1),2.0f * i / (255-1)-1.0f);


    }
public slots:
    void mapInit();
    void createMap();
    void setFreq(int freq);
    void start();
signals:
    void progress(int i );
    void endCreateMap(QList <point*> i);
};

#endif // WORLD_H
