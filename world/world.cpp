#include "world.h"
#include "QThread"
#include "QDebug"
#include "iostream"


void World::createMap()
{

    //QList <point*> map;

    perlin.perlin2dInit();
    float proress = y/100.0f;
    map.clear();
    qDebug()<<this->freq<<x<<y;;
    for (int yt = 0; yt < y; yt++) {
        emit progress(static_cast<int>((yt+1)/proress+proress));
        for (int xt = 0; xt < x; xt++) {
//            std::cout<<xt<<" " <<yt<<" ";
//            std::cout<<yt*x+xt;
            map.append(new point(xt,yt));
            float iii = perlin.getNoise(xt/(freq*1),yt/(freq*1),8,0.5f);
            int i =static_cast<int> (iii* 255.0f+ 128.0f) & 255;
            map.last()->setLand(i);
            map.last()->p=pixelPosToViewPos(xt,yt,i);
        }
//        std::cout<<"\r\n";
    }
    emit endCreateMap(map);
}

void World::setFreq(int freq)
{
    this->freq = static_cast<float>(freq)+0.000001f;
}
float World::getFreq()
{
    return freq;
}

void World::mapInit()
{

}
void World::start()
{
    qInfo() << "world thread:" << QThread::currentThreadId();
    mapInit();
    createMap();
}

World::World(QObject *parent) : QObject(parent)
{
    y = 0;
    x = 0;
}

void World::setHW(int height, int width)
{
    this->y = height;
    this->x = width;
    //    ggg a(*(ggg*)bls());
}

void World::bls()
{
    //    int uyhgjh=0;
    //    return (void*)&uyhgjh;
}

World::World(int height, int width)
{
    setHW(height,width);
}

World::~World()
{

}
