#ifndef PERLIN2D_H
#define PERLIN2D_H
#include <QByteArray>

class Vector {
public:
    float x;
    float y;

    Vector(float x, float y) {
        this->x = x;
        this->y = y;
    }
    Vector(){
        this->x = 0.f;
        this->y = 0.f;
    }
};
class Perlin2D: public Vector
{
private:
    QByteArray permutationTable;

    float getNoise(float x, float y);
    float qunticCurve(float t);
    float lerp(float a, float b, float t);
    float dot(Vector a, Vector b);
    Vector getPseudoRandomGradientVector(int x, int y);

public:
    float getNoise(float fx, float fy, int octaves, float persistence = 0.5f);
    Perlin2D();
    ~Perlin2D();
    void perlin2dInit();
};

#endif // PERLIN2D_H
